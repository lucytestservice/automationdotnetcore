﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Generic
{
    //There is 5 type of constraint
    //Where T : IComparableInterface
    //Where T : ProductClass (included its derrived class)  
    //Where T : struct (value type)
    //Where T : class (where T is a object with class type)
    //Where T : new() -> T is an instance
    public class GenericWithConstraintwhere<T> where T : IComparable, new()
    {
        public int Max(int a, int b)
        {
            return a > b ? a : b;
        }

        public T Max(T a, T b)
        {
            return a.CompareTo(b) > 0 ? a : b;
        }

        public void DoSomeThing(T value)
        {
            var obj = new T();
        }
    }
}
