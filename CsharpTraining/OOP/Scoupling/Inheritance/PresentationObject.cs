﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Inheritance
{
    public class PresentationObject
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public void Copy()
        {
            Console.WriteLine("Copied Object!");
        }

        public void Duplicate()
        {
            Console.WriteLine("Duplicated Object!");
        }
    }
}
