﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.DateTimePractices
{
    public class DateTimePractice
    {
        //Date Time Are Immutable
        public static void Practice()
        {
            var dateTime = new DateTime(2019, 9, 30);
            var now = DateTime.Now;
            var today = DateTime.Today;

            Console.WriteLine($"Today: {today}");
            Console.WriteLine($"Now: {now}");
            Console.WriteLine($"Hour: {now.Hour}");
            Console.WriteLine($"Minute: {now.Minute}");

            var tomorrow = now.AddDays(1);
            var yesterday = now.AddDays(-1);

            Console.WriteLine($"Tomorrow is {tomorrow}");
            Console.WriteLine($"Yesterday is {yesterday}");

            //Convert to String

            Console.WriteLine($"Long date { now.ToLongDateString()}");
            Console.WriteLine($"Short date {now.ToShortDateString()}");
            Console.WriteLine($"Long time {now.ToLongTimeString()}");
            Console.WriteLine($"Short time {now.ToShortTimeString()}");
            Console.WriteLine(now.ToString("yyyy-MM-dd HH:mm:ss")); // can enter any pattern
        }

        public static void PracticeTimeSpan()
        {
            //Create TimeSpan object
            var timeSpan = new TimeSpan(1, 2, 3);
            var timeSpan2 = new TimeSpan(1, 0, 0);
            var timeSpan3 = TimeSpan.FromHours(2);

            var start = DateTime.Now;
            var end = start.AddMinutes(2);
            Console.WriteLine($"Duration is {end - start}");

            // Read TimeSpan Properties
            Console.WriteLine($"Minute : {timeSpan.Minutes}");
            Console.WriteLine($"Total Minute : {timeSpan.TotalMinutes}");

            // Add 2 TimeSpan
            Console.WriteLine(timeSpan.Add(timeSpan2));

            // Subtract
            Console.WriteLine(timeSpan.Subtract(timeSpan2));

            //ToString()
            Console.WriteLine(timeSpan.ToString()); // In case you dont use WriteLine

            // Parse()
            Console.WriteLine(TimeSpan.Parse("01:02:03"));
        }
    }
}
