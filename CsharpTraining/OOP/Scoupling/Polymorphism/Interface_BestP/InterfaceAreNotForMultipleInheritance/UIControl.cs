﻿using System;
using System.Collections.Generic;
using System.Text;
using CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP.InterfaceAreNotForMultipleInheritance;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{

    public class UIControl
    {
        public string Id { get; set; }
        public Size Size { get; set; }

        public Position TopLeft { get; set; }

        public virtual void Draw()
        {

        }

        public void Focus()
        {
            Console.WriteLine("Received focus");
        }
    }
}
