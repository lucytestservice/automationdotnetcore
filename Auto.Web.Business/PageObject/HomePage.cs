﻿using AutoCore;
using AutoCore.UITesting;
using OpenQA.Selenium;

namespace Auto.Web.Business.PageObject
{
    public class HomePage
    {
        public IWebElement FindTxt => DriverFactory.Instance.FindElement(By.XPath("//input[@name='q']"));
    }
}