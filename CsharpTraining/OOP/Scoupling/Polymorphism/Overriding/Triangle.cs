﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Overriding
{
    public class Triangle : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("Draw Triangle");
        }
    }
}
