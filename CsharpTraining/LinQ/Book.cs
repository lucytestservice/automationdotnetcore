﻿using System;
using System.Text;

namespace CsharpTraining.LinQ
{
    public class Book
    {
        public string Title { get; set; }

        public float Price { get; set; }
    }
}
