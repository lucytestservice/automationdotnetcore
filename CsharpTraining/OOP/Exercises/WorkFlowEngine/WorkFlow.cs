﻿using System;
using System.Collections.Generic;

namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public class WorkFlow
    {
        private readonly IList<IActivityController> _listActs;

        public WorkFlow()
        {
            _listActs = new List<IActivityController>();
        }

        public void Start()
        {
            foreach (var activity in _listActs)
            {
                activity.Execute();
            }
        }

        public void AddActivity(IActivityController activity)
        {
            _listActs.Add(activity);
        }
    }
}