﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public class WorkflowEngine
    {
        public static void Run(WorkFlow workflow)
        {
            workflow.Start();
        }
    }
}
