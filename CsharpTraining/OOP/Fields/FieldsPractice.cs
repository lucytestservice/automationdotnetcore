﻿using System;
using System.Collections.Generic;
using System.Text;
using CsharpTraining.OOP.ClassPractice;

namespace CsharpTraining.OOP.Fields
{
    public class FieldsPractice
    {
        //Init class field normally
        public List<Order> orders = new List<Order>();

        // Or protect it by readonly keyword to prevent data lost when somebody remove data from order field
        readonly List<Order> orders2 = new List<Order>();

        public static void Practice()
        {
            FieldsPractice fields = new FieldsPractice();
            fields.orders.Add(new Order());
            fields.orders.Add(new Order());
            Console.WriteLine(fields.orders.Count);

            fields.Promote(); //Make order newly initialized to empty list => which will cause unexpected bug
            Console.WriteLine(fields.orders.Count);


        }

        public void Promote()
        {
            orders = new List<Order>();
        }

        public void Promote2()
        {
            //orders2 = new List<Order>(); // can not update or make changes to readonly fields
        }
    }
}
