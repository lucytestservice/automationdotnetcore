﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class NullCoalestingOperator
    {
        public static void Practice()
        {
            string a = null;
            string b = "dang";
            string value = a ?? b; // if a not null then return a otherwise return b
        }
    }
}
