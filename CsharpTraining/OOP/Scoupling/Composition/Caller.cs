﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Composition
{
    /**
     * Coupling la khop noi giua cac part ( connectors )
     */
    public class Caller
    {
        // Composition is an inheritance but it loose coupling (thao' long? cac khop noi) so we can easily use inheritance
        //compistion is very similar with inheritance in the way we approach
        // But it can be more flexible while compisition can associate multiple class
        // While Inheritance 1 class can only inherit from one base class 
        public static void Practice()
        {
            var dbMigrator = new DBMigrator(new LoggerClass());

            var logger = new LoggerClass();
            var installer = new Installer(logger);

            dbMigrator.Migrate();
            installer.Install();
        }
    }
}
