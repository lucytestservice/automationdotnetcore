﻿namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public class UpdateStatus : IActivityController
    {
        private const string _message = "UPDATE STATUS OF MESSAGE....";

        public void Execute()
        {
            System.Console.WriteLine(_message);
        }
    }
}