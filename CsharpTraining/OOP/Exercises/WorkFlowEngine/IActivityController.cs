﻿namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public interface IActivityController
    {
        void Execute();
    }
}