﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class TernaryOperator
    {
        /**
         * condition ? expression_if_true : expression_if_false;

         */

        // Nested ternary operator
        public static float Clamp(float val, float min, float max)
        {
            return (val < min) ? min : (val > max) ? max : val;
        }
    }
}
