﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Dynamic
{
    public class Practice
    {
        public static void Caller()
        {
            object obj = "dang";

            //Modern dynamic feature from dotnet
            obj.GetHashCode();

            //To replace this Reflection feature
            //var methodInfo = obj.GetType().GetMethod("GetHashCode");
            //methodInfo.Invoke(null, null);

            int i = 6;
            dynamic y = i;
            long f = y;
            dynamic c = i + f;

        }
    }
}
