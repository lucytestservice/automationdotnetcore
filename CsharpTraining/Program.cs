﻿using CsharpTraining.EnumPractices;
using CsharpTraining.ValueTypeAndReferenceType;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CsharpTraining
{
    using CsharpTraining.Delegate;
    using OOP.ClassPractice;
    class Program
    {
        // Everytime you see parameter take IEnumrable -> supply it a List or Array that can be iterate

        static void Main(string[] args)
        {
            Console.WriteLine("Engine executing......");

            //Understanding.Practice();

            //BuiltInSupport.BuiltInMethod.practice();

            //ArrayPractices.ArrayPractices.Practice();

            //ListPractices.ListPractices.Practice();


            //DateTimePractices.DateTimePractice.PracticeTimeSpan();

            //FileIO.DirectoryPractice.Practice();

            //FileIO.PathPractice.Practice();

            /* OOP.ClassPractice.CallerClass.Practice();
               OOP.Classe.HandlPractice.CallerClass.ObjectInitializer();*/

            //OOP.MethodPractice.MethodMultiParams.Practice();

            //BestPracticeConversionBeforeCovert.Practice();5

            //OOP.Fields.FieldsPractice.Practice();

            //OOP.AccessModifier.CallerClass.Practice();

            //OOP.PropertiesPractice.PropertiesPractice.Practice();

            //OOP.Indexer.IndexerPractice.Practice();

            //OOP.Exercises.CallerClass.Practice();

            //OOP.Scoupling.Inheritance.Caller.Practice();

            //OOP.Scoupling.Composition.Caller.Practice();

            //OOP.Scoupling.Inheritance.UpCastingDownCasting.CallerClass.Practice();

            //ValueTypeAndReferenceType.UnboxingAndBoxing.Practice();

            //OOP.Scoupling.Polymorphism.Caller.Practice();

            //OOP.Exercises.CallerClass.Practice();

            //Generic.Caller.Practice();


            //var processor = new PhotoProcessor();
            //var filters = new PhotoFilters();
            //Action<Photo> filterHandler = filters.ApplyBrightness; //Point to ApplyBrightness method
            ////Use delegate overloading to tech several methods altogether
            //filterHandler += filters.ApplyContrast;
            //filterHandler += filters.Resize;
            //filterHandler += CustomPhotoFilter.FilterRedEyes;

            //processor.Process("/asda.png", filterHandler);

            //Lambda.LambdaClass.Practice();

            //Events.Practice.Caller();

            //ExtensionMethods.Caller.Practice();

            //LinQ.Practice.Caller();

            //NullableType.Practice.Caller();

            //Dynamic.Practice.Caller();

            //ExceptionHandling.Practice.Caller();

            AsyncAwait.Practice.Caller();
        }
    }
}