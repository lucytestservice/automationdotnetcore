﻿namespace CsharpTraining.OOP.Scoupling.Polymorphism
{
    public class SmsNotificationChannel : INotificationChannel
    {
        public void Send(Message message)
        {
            System.Console.WriteLine($"Sending sms....{message}");
        }
    }
}