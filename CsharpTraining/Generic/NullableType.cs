﻿namespace CsharpTraining.Generic
{
    //In Csharp, Value type can not be null. So we use this technique to make is can be nullable
    public class NullableType<T> where T : struct
    {
        private object _value;

        public NullableType()
        {

        }
        public NullableType(T value)
        {
            _value = value; //Boxing to object type
        }

        public bool HasValue
        {
            get { return _value != null; }
        }

        public T GetValueOrDefault()
        {
            if (HasValue)
                return (T)_value;
            return default(T);
        }

    }
}
