﻿using System;

namespace CsharpTraining.ExceptionHandling
{
    public class VideoException : Exception
    {
        public VideoException(string msg, Exception innterException)
            : base(msg, innterException)
        {

        }
    }
}
