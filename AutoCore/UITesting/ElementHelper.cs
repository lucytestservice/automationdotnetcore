using System.Threading;
using OpenQA.Selenium;

namespace AutoCore.UITesting
{
    public static class ElementHelper
    {   
        public static void Type(this IWebElement webElement, string text, int second = 3)
        {
            if (DriverFactory.Instance.WaitForEnabled(webElement, second))
            {
                webElement.SendKeys(text);
            }
            else
            {
                // TODO: Log: The Element is not enabled to type text
            }
        }

        public static void Click(this IWebElement webElement, int second = 3)
        {
            if (DriverFactory.Instance.WaitForEnabled(webElement, second))
            {
                webElement.Click();
            }
            else
            {
                // TODO: Log: The Element is not enabled to click
            }
        }

        public static void DoubleClick(this IWebElement webElement, int second = 3)
        {
            if (DriverFactory.Instance.WaitForEnabled(webElement, second))
            {
                webElement.Click();
                Thread.Sleep(800);
                webElement.Click();
            }
            else
            {
                // TODO: Log: The Element is not enabled to double click
            }
        }

        public static void Selected(this IWebElement webElement, int second = 3)
        {
            if (webElement.Selected) return;
            if (DriverFactory.Instance.WaitForEnabled(webElement, second))
            {
                webElement.Click();
            }
        }
    }
}