﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Delegate
{
    public class PhotoProcessor
    {
        /**
         * delegate method is a kind of type which represent a certain method
         * In other word, this method can be treated as a object type, value, param
         */
        //public delegate void PhotoFilterHandler(Photo photo);

        public void Process(string path, Action<Photo> photoFilterHandler)
        {
            var photo = Photo.Load(path);

            //normal approach
            //var filters = new PhotoFilters();
            //filters.ApplyBrightness(photo);
            //filters.ApplyContrast(photo);
            //filters.Resize(photo);

            //Custom Delegate approach
            //photoFilterHandler(photo);

            //Using delegate built-in of .NET
            //System.Action<> //This can point upto 16 methods and Action just point to methods which return void
            //System.Func<> //This can point upto 16 methods and Func just point to methods return value
            photoFilterHandler(photo);


            photo.Save();
        }
    }
}
