﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.ArrayPractices
{
    public class ArrayPractices
    {
        /**
         * Attribute: Length
         *
         * Popular methods:
         * 1. 
         * 2.
         * 3.
         * 4.
         * 5.
         * 
         */

        public static void Practice()
        {
            var a = new[]{ 1,2,3,4,5}; //Short declare
            // Length attribute
            Console.WriteLine($"Length of array is: {a.Length}");

            // Identify IndexOf()
            Console.WriteLine($"Index of 3 is {Array.IndexOf(a, 3)}");

            // Clear()
            Array.Clear(a, 3, 1);
            Console.WriteLine($"Clearing item index 3 to its default value (get default value from default() method built-in ) => to zero : {a[3]}");

            // Copy()
            var b = new int[2];
            Array.Copy(a,b, 2);
            Console.WriteLine(b[0]);
            Console.WriteLine(b[1]);


            // Sort()
            Array.Sort(a);
            foreach(var val in a)
            {
                Console.WriteLine(val);
            }

            // Reverse()
            Array.Reverse(a);
            foreach (var val in a)
            {
                Console.WriteLine(val);
            }

        }
    }
}
