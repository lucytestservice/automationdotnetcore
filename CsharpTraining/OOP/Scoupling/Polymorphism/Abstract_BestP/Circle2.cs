﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Abstract
{
    public class Circle2 : Shape2
    {
        protected override void Draw()
        {
            Console.WriteLine("Implement Draw Circle");
        }
    }
}
