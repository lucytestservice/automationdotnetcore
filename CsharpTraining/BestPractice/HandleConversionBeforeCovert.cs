﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.BestPractice
{
    public class HandleConversionBeforeCovert
    {
        public static void Practice()
        {
            ConvertWithTrick();
        }

        public static void ConvertWithoutTryCatch()
        {
            var num = int.Parse("abc");
        }

        public static void ConvertWithTrick()
        {
            int num;
            var result = int.TryParse("abc", out num);
            if(result)
                Console.WriteLine(@$"Value is {num}");
        }
    }
}
