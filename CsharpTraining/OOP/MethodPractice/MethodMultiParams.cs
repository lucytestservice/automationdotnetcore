﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.MethodPractice
{
    public class MethodMultiParams
    {
        public static void Practice()
        {
            MultiParams();
            var a = 10;
            RefModifier(ref a);
            Console.WriteLine(a);

            OutModifier(out a);
            Console.WriteLine(a);

            //Overloading method

            Point point = new Point(10,20);
            point.Move(new Point(20, 30));
            Console.WriteLine("Point is at {0} and {1}", point.X, point.Y);

            point.Move(30, 40);
            Console.WriteLine("Point is at {0} and {1}", point.X, point.Y);
        }

        public static void MultiParams(params string[] strings)
        {
            foreach (var stringval in strings)
            {
                Console.WriteLine(stringval);
            }
        }

        public static void RefModifier(ref int a)
        {
            //This a will map to the reference outside and make it changes its value
            a += 10;
        }

        public static void OutModifier(out int a)
        {
            a = 50;
        }
    }
}
