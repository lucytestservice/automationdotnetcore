﻿using System.Collections.Generic;

namespace CsharpTraining.LinQ
{
    public class BookRepository
    {
        public IEnumerable<Book> GetBooks()
        {
            return new List<Book>
            {
                new Book() {Title = "Book1", Price=10},
                new Book() {Title = "Book5", Price=15},
                new Book() {Title = "Book2", Price=20},
                new Book() {Title = "Book4", Price=25},
                new Book() {Title = "Book6 star", Price=25},
                new Book() {Title = "Book9 star", Price=25},
                new Book() {Title = "Book7 star", Price=25},
                new Book() {Title = "Book3 Special", Price=30},
            };
        }
    }
}
