﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class RelationalOperator
    {
        public static void practice1()
        {
            bool result = "a" == "b"; // Returns false.
            result = "a" == "a"; // Returns true.
            result = 1 == 0; // Returns false.
            result = 1 == 1; // Returns true.
            result = false == true; // Returns false.
            result = false == false; // Returns true.


            bool result2 = 1 == 1.0;// Returns true because there is an implicit cast from int to double.
            //result2 = new Object() == 1.0; // Compiling error. There is no implicit casting for object to int

            //result2 = MyStruct.AsInt() == 1; // Calls AsInt() on MyStruct and compares the resulting int with 1.



        }
    }
}
