﻿using CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP;
using System;
using System.Collections.Generic;
using System.Text;
using CsharpTraining.OOP.Scoupling.Polymorphism.Abstract;
using CsharpTraining.OOP.Scoupling.Polymorphism.Overriding;

namespace CsharpTraining.OOP.Scoupling.Polymorphism
{
    class Square : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("Draw Square");
        }
    }


    public class Caller
    {
        public static void Practice()
        {
            List<Shape> listShape = new List<Shape>();
            listShape.Add(new Circle());
            listShape.Add(new Triangle());
            listShape.Add(new Square());

            Canvas canvas = new Canvas();
            canvas.DrawShape(listShape);

            //Abstract is to indicate that component is not implement

            Circle2 circle2 = new Circle2();
            circle2.InvokeDraw();


            //Interface 
            /* DBMigrator dB = new DBMigrator(new ConsoleLogger());
             dB.Migrate();

             DBMigrator dB1 = new DBMigrator(new FileLogger(@"C:\log.txt"));
             dB1.Migrate();*/

            var encoder = new VideoEncoder();
            encoder.RegisterNotificationChannel(new MailNotificationChannel());
            encoder.RegisterNotificationChannel(new SmsNotificationChannel());
            encoder.Encode(new Video());
        }
    }
}
