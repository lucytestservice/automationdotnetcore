﻿using System;

namespace CsharpTraining.OOP.Scoupling.Composition
{
    public class DBMigrator
    {
        private readonly LoggerClass _logger;

        public DBMigrator(LoggerClass logger)
        {
            _logger = logger;
        }

        public void Migrate()
        {
            _logger.Log("Migrate");
        }
    }
}
