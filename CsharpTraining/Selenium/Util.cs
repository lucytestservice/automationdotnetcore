﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace CsharpTraining.Selenium
{
    public class Util
    {/*
        public static void clickDateCalender(IWebDriver driver, string date)
        {
            IWebElement element = driver.FindElement(By.CssSelector("#travel_date"));
            *//*IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].scrollIntoView();", element);*//*

            element.Click();

            //Find total days display in the calendar
            IList<IWebElement> days = driver.FindElements(By.XPath("//*[@class='datepicker-days']//following-sibling::td"));
            int totalDays = days.Count;
            Console.WriteLine(totalDays);

            // Calculate the landmark = totaldays - averageDaysInMonth (28)
            int landmark = totalDays - 28;

            // Click on the element containing the same text that we want to click on
            IList<IWebElement> dateElement = driver.FindElements(By.XPath($"//*[@class='datepicker-days']//following-sibling::td[text()='{date}']"));
            Console.WriteLine(dateElement.Count);

            // If found duplicated elements
            // Check the logic 
            if (dateElement.Count > 1)
            {
                // If date less than or equal the landmark
                if (int.Parse(date) <= landmark)
                {
                    dateElement[0].Click();
                }
                // Otherwise click the second
                else
                {
                    dateElement[1].Click();
                }
            }
            else
            {
                dateElement[0].Click();
            }
        }

        public static void slideTo(IWebDriver driver, int targetValue, int maxValue, int minValue)
        {
            IWebElement slider = driver.FindElement(By.XPath("//*[@id='slider1']//input"));

            int pixelX = slider.Size.Width;
          
            Actions actions = new Actions(driver);

            actions.MoveToElement(
                    slider,
                    pixelX * (targetValue - minValue) / (maxValue - minValue),
                    0
                )
                .Click().Build().Perform();
            
            Thread.Sleep(3000);
        }*/

    }
}
