﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsharpTraining.ExceptionHandling
{
    public class Practice
    {
        public static void Caller()
        {
            HandlingCalculato();
            HandlingStuckMemoryWhenException();
            HandlingStuckMemoryWhenExceptionInModernWay();

            try
            {
                GetVideos();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void HandlingCalculato()
        {
            var calculator = new Calculator();
            int a;
            try
            {
                a = calculator.Divide(10, 0);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("You can not divide a number to zero");
            }
            catch (Exception e)
            {
                Console.WriteLine("Sorry! There are some error");
            }
        }

        public static void HandlingStuckMemoryWhenException()
        {
            StreamReader streamReader = null;
            try
            {
                streamReader = new StreamReader(@"C:\log.txt");
                var content = streamReader.ReadToEnd();
                throw new Exception("BUGGGG");
            }
            catch (Exception e)
            {
                Console.WriteLine("There are some error happen!");
            }
            finally
            {
                //Kill the process and memory of object
                if (streamReader != null)
                    streamReader.Dispose();
            }
        }

        public static void HandlingStuckMemoryWhenExceptionInModernWay()
        {
            try
            {
                using (var streamReader = new StreamReader(@"C:\ehe.txt"))
                {
                    var content = streamReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("There are some error happen!");
            }
        }

        public static List<Video> GetVideos()
        {
            try
            {
                throw new Exception("BUGSSSSS");
            }
            catch (Exception e)
            {
                //Wrap the original exception
                throw new VideoException("Video is not able to get!", e);
            }
            return new List<Video>();
        }
    }
}
