﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.ValueTypeAndReferenceType
{
    public class Understanding
    {
        public static void ChangeNumber(int number) //this will copy a new value type object
        {
            number += 2;
        }

        public static void ChangeReference(int[] a)
        {
            a[0] = 20;
        }

        public static void Practice()
        {
            //Value type and its value were created directly on stack and will be clear once it go out of scope
            //Copy value type
            var a = 5;
            var b = a;
            b++;
            //Now value of a is copied to b and they will not impact each other (they are imutable)
            Console.WriteLine(string.Format("a and b :{0} {1}", a, b));
            ChangeNumber(a);
            ChangeNumber(b);
            // Both a and b can not be change because the method copy a new instance so the original can not be affected
            Console.WriteLine(a);
            Console.WriteLine(b);

            //while reference type was created on stack and reference value was created on heap (have its address)
            //for example create some mutable type object
            var array1 = new int[3] { 1, 2, 3 };
            var array2 = array1;
            array2[0] = 10; // at this time reference value[0] was change to 10 and array1 and array2 both referred to it
            Console.WriteLine(string.Format("Value after process {0} {1}", array1[0], array2[0]));
            ChangeReference(array2);
            Console.WriteLine(string.Format("Value after process {0} {1}", array1[0], array2[0]));


            //String is especially immutable
        }
    }
}
