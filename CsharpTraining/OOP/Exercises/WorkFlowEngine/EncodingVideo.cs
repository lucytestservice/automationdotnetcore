﻿namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public class EncodingVideo : IActivityController
    {
        private const string _message = "CACULATE TO ENCODE THE VIDEO....";

        public void Execute()
        {
            System.Console.WriteLine(_message);
        }
    }
}