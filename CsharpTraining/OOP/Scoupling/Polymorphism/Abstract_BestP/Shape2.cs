﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Abstract
{

    public abstract class Shape2
    {
        public int Width { get; set; }
        public int Heith { get; set; }

        protected abstract void Draw();

        public void InvokeDraw()
        {
            //We use abstract method as a trick here -> the Draw() method will behave base on the derrived class implementing it
            //So we just need to call InvokeDraw() everytime we call
            Draw();
        }
    }
}
