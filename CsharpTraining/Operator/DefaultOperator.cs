﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class DefaultOperator
    {
        /**
         The built-in primitive data types, such as char, int, and float, as well as user-defined types declared with struct,
        or enum. Their default value is new T() :

        default(int) // 0
        default(DateTime) // 0001-01-01 12:00:00 AM
        default(char) // '\0' This is the "null character", not a zero or a line break.
        default(Guid) // 00000000-0000-0000-0000-000000000000
        default(MyStruct) // new MyStruct()

        // Note: default of an enum is 0, and not the first *key* in that enum
        // so it could potentially fail the Enum.IsDefined test

        default(MyEnum) // (MyEnum)0


        Any class, interface, array or delegate type. Their default value is null :
        default(object) // null
        default(string) // null
        default(MyClass) // null
        default(IDisposable) // null
        default(dynamic) // null

         */
    }
}
