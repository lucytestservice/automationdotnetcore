﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Indexer
{
    public class HttpCookieClass
    {
        //Indexer is the same with property but we can use it for 
        // a data structure like class. Purpose is to retrieve the data we want

        private readonly Dictionary<string, string> _dictionary;

        public HttpCookieClass()
        {
            _dictionary = new Dictionary<string, string>();
        }

        public string this[string key]
        {
            get { return _dictionary[key]; }
            set { _dictionary[key] = value; }
        }
    }
}
