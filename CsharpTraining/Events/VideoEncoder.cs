﻿using System;
using System.Threading;

namespace CsharpTraining.Events
{
    public class VideoEventArgs : EventArgs
    {
        public Video Video { get; set; }
    }

    public class VideoEncoder
    {
        //1. Define a delegate as the contract/signature between the publisher and the subscriber
        //2. Define an event base on the delegate
        //3. Raise the event


        //Delegate as well as the signature, other service must implement this signature (Oldschool style - custom delegate)
        //public delegate void VideoEncoderEventHandler(object source, VideoEventArgs args);
        //public event VideoEncoderEventHandler VideoEncoded;

        //Built-in Delegate EventHandler of dot net
        //EventHandler
        //EventHandler<>
        public event EventHandler<VideoEventArgs> VideoEncoded; //You can remove the generic if you dont need data from event

        protected virtual void OnVideoEncoded(Video video)
        {
            if (VideoEncoded != null)
            {
                VideoEncoded(this, new VideoEventArgs() { Video = video });
            }
        }

        public void Encode(Video video)
        {
            Console.WriteLine("Encoding video......");

            Thread.Sleep(3000);

            Console.WriteLine("Completed!");

            OnVideoEncoded(video);
        }
    }
}