﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.AccessModifier
{
    public class CallerClass
    {
        public static void Practice()
        {
            AccessModifierPractice amp = new AccessModifierPractice();
            Console.WriteLine(amp.GetBirthDate());
        }
    }
}
