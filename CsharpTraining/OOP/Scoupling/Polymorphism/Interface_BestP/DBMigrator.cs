﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{

    public class DBMigrator
    {
        private readonly ILogger _logger;

        public DBMigrator(ILogger logger)
        {
            _logger = logger;
        }
        public void Migrate()
        {
            _logger.LogInfo($"Migration start at {DateTime.Now}");
            Thread.Sleep(2000);
            _logger.LogInfo($"Migration finish at {DateTime.Now}");
        }
    }
}
