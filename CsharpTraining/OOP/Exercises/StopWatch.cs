﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Exercises
{
    public class StopWatch
    {
        private static DateTime Starttime { get; set; }
        private static DateTime Stoptime { get; set; }

        public static double Duration
        {
            get
            {
                return (Starttime - Stoptime).Duration().TotalSeconds;
            }
        }

        public static void Start()
        {
            Starttime = DateTime.Now;
        }

        public static void Stop()
        {
            Stoptime = DateTime.Now;
        }


    }
}
