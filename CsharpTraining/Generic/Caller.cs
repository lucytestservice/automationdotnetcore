﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Generic
{

    public class Caller
    {
        public static void Practice()
        {
            Book book = new Book();

            var intList = new GenericList<int>();
            intList.Add(100);

            var bookList = new GenericList<Book>();
            bookList.Add(new Book());

            var genericDict = new GenericDictionary<string, Book>();
            genericDict.Add("stuff", new Book());

            var number = new NullableType<int>();
            Console.WriteLine(number.GetValueOrDefault());
            number = new NullableType<int>(10);
            Console.WriteLine(number.GetValueOrDefault());

        }
    }
}
