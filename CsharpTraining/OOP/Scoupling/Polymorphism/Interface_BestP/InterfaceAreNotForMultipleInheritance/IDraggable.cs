﻿namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{
    internal interface IDraggable
    {
        void Drag();
    }
}