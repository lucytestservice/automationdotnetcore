﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsharpTraining.FileIO
{
    public class PathPractice
    {
        public static void Practice()
        {
            //Path
            /**
             * GetDirectoryName()
             * GetFileName()
             * GetExtension()
             * GetTempPath()
             * 
             */

            //with normal string practice below
            var path = @"D:\csharp\automation\lucy_automation.sln";

            var stringDot = path.IndexOf(".");
            var extension = path.Substring(stringDot);
            Console.WriteLine(extension); // display .sln

            //But we can do better by using Path class
            Console.WriteLine(Path.GetExtension(path));
            Console.WriteLine($"Get file name without extension: {Path.GetFileNameWithoutExtension(path)}");
            Console.WriteLine($"Get file name with extension: {Path.GetFileName(path)}");
            Console.WriteLine($"Get directory name: {Path.GetDirectoryName(path)}");
        }
    }
}
