﻿using System;
using System.Threading;

namespace CsharpTraining.Events
{
    public class MailService
    {
        public void OnVideoEncoded(object source, VideoEventArgs e)
        {
            Console.WriteLine("MailService sending an email...." + e.Video.Title);
            Thread.Sleep(2000);
            Console.WriteLine("Completed sending email!");
        }
    }
}
