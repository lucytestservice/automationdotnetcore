﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class SizeOfOperator
    {
        /**
         * Returns an int holding the size of a type* in bytes.

            *Only supports certain primitive types in safe context
         */
        public void Practice()
        {
            int a = sizeof(bool); // Returns 1.
            a = sizeof(byte); // Returns 1.
            a = sizeof(sbyte); // Returns 1.
            a = sizeof(char); // Returns 2.
            a = sizeof(short); // Returns 2.
            a = sizeof(ushort); // Returns 2.
            a = sizeof(int); // Returns 4.
            a = sizeof(uint); // Returns 4.
            a = sizeof(float); // Returns 4.
            a = sizeof(long); // Returns 8.
            a = sizeof(ulong); // Returns 8.
            a = sizeof(double); // Returns 8.
            a = sizeof(decimal); // Returns 16.
        }


        /**
         * In an unsafe context, sizeof can be used to return the size of other primitive types and structs.
            public struct CustomType
            {
             public int value;
            }
            static void Main()
            {
             unsafe
             {
             Console.WriteLine(sizeof(CustomType)); // outputs: 4
             }
            }
         */
    }
}
