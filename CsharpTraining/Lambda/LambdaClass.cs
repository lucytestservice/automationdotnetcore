﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Lambda
{
    class LambdaClass
    {
        public static void Practice()
        {
            //Console.WriteLine(Square(5));

            //Lambda expression for Square() method is shoul in format arg => expression
            // number => number * number
            //We can put above lambda expression to Delegate

            //Func<int, int> square = number => number * number;

            const int factor = 5;

            Func<int, int> multiplier = n => n * factor;

            Console.WriteLine(multiplier(10));

            var books = new BookRepository().GetBooks();

            //var cheapbooks = books.FindAll(BookRepository.IsCheaperThan10Dollar);
            var cheapbooks = books.FindAll(book => book.Price < 10);

            foreach (var book in cheapbooks)
            {
                Console.WriteLine(book.Title);
            }
        }

        public static int Square(int number)
        {
            return number * number;
        }
    }
}
