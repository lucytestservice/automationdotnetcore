﻿namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}