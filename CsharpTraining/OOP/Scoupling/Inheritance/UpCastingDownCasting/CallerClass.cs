﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Inheritance.UpCastingDownCasting
{
    public class CallerClass
    {
        /**
         * NOTE: Using 'as' keyword to check whether we can downcasting a object to another object type
         * Using 'is' to check whether the object is certain type
         */
        public static void Practice()
        {
            DerrivedClass derrived = new DerrivedClass();
            BaseClass basec = derrived; //Upcasting derrived to base

            DerrivedClass derrived2 = (DerrivedClass)basec; //Downcasting base to derrived

            //Car car = (Car)derrived2; //Invalid casting
            //Check 

            DerrivedClass derrivedClass = new DerrivedClass();
            BaseClass baseClass = derrivedClass;

            derrivedClass.Width = 200;
            baseClass.Width = 100;
            Console.WriteLine(derrivedClass.Width);


        }

        public static void Practice2()
        {
            var list = new List<BaseClass>();
        }
    }
}
