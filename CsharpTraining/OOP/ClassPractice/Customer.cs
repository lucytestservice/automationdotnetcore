﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.ClassPractice
{
    public class Customer
    {
        public int Id;
        public string Name;
        public List<Order> orders;
        public Customer()
        {
            orders = new List<Order>();
        }

        public Customer(int Id) : this() //Using this() to make one constructor depend on another constructor
        {
            this.Id = Id;
        }

        public Customer(int Id, string Name) : this(Id) //We can also pass any parameter to the this() dependency
        {
            this.Name = Name;
            this.Id = Id;
        }

        new public void ToString()
        {
            Console.WriteLine(this.Name);
            Console.WriteLine(this.Id);
        }
    }
}
