﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.EnumPractices
{
    public enum NewEnumType
    {
        TYPE1 = 1,
        TYPE2 = 2,
        TYPE3 = 3
    }
    public class EnumPractice
    {
        public static void Practice()
        {
            var a = NewEnumType.TYPE1;
            Console.WriteLine((int)a);
            Console.WriteLine(a.ToString()); //Cast back to enum name by enum value which is string

            var b = 1;
            var c = (NewEnumType)b;
            Console.WriteLine(c);

            //Using enum to mapping trick
            var mapName = "TYPE3";
            var enumName = (NewEnumType)Enum.Parse(typeof(NewEnumType), mapName); //get enum name is TYPE3
            Console.WriteLine((int)enumName); //Cast to value again

        }
    }
}
