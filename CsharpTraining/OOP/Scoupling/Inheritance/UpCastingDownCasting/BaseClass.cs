﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Inheritance.UpCastingDownCasting
{
    public class BaseClass
    {
        public int Width { get; set; }
        public int Heith { get; set; }

        public int X { get; set; }
        public int Y { get; set; }

        public void Draw()
        {

        }
    }
}
