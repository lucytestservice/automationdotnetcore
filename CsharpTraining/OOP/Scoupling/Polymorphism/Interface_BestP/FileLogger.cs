﻿using System.IO;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{
    public class FileLogger : ILogger
    {
        private readonly string _path;
        public FileLogger(string path)
        {
            _path = path;
        }
        public void LogError(string message)
        {
            Log(message, "ERROR");

        }

        public void LogInfo(string message)
        {
            Log(message, "INFO");
        }

        public void Log(string message, string messageType)
        {
            // using is a technique that can automatically handle exception
            // when working with file (because file resource is not imlemented by the compiler) 
            // And close the file even there is a exception when writing file
            using (var streamWriter = new StreamWriter(_path, true))
            {
                streamWriter.WriteLine(message);
            }
        }
    }
}
