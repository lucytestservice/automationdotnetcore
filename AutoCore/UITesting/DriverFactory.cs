using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using AutoCore.CommonUtilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;
using static AutoCore.UITesting.BrowserType;

namespace AutoCore.UITesting
{
    public static class DriverFactory
    {
        private static IWebDriver _webDriver = null;

        public static IWebDriver Instance => _webDriver ??= InitializeInstance();

        private static IWebDriver InitializeInstance(string browserType = "Chrome")
        {
            if (_webDriver != null) return _webDriver;
            
            if (browserType == Firefox.Value)
            {
                _webDriver = new FirefoxDriver();
            }
            else if (browserType == Edge.Value)
            {
                _webDriver = new EdgeDriver();
            }
            else if (browserType == InternetExplorer.Value)
            {
                _webDriver = new InternetExplorerDriver();
            }
            else if (browserType == Safari.Value)
            {
                _webDriver = new SafariDriver();
            }
            else
            {
                _webDriver = new ChromeDriver();
            }
            
            _webDriver.Manage().Cookies.DeleteAllCookies();
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            _webDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
            _webDriver.Manage().Window.Size = new Size(1024, 768);
            return _webDriver;
        }
        
        public static void Dispose()
        {
            using (_webDriver)
            {
                if (_webDriver == null) return;
                _webDriver.Quit(); //Quit() will be redirected to Dispose() in other words..
                _webDriver = null;
            }
        }
    }

    public class BrowserType : ReferenceEnumType
    {
        private BrowserType(string browserName) : base(browserName)
        {
        }

        public static readonly BrowserType Chrome = new BrowserType("CHROME");
        public static readonly BrowserType Firefox = new BrowserType("FIREFOX");
        public static readonly BrowserType InternetExplorer = new BrowserType("INTERNETEXPLORER");
        public static readonly BrowserType Edge = new BrowserType("EDGE");
        public static readonly BrowserType Safari = new BrowserType("SAFARI");
    }
}