﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.AsyncAwait
{
    public class Practice
    {
        public static void Caller()
        {
            /** method1():
             *   statement1
             *   a = statement2
             *   statement3
             *   statement4 = a; - need result of statement 2
             * 
             * Look at above example statement 2 is blocker of statement 4, so why do we have to wait for statement 2 to be completed
             * before statement3 while they are not relate to each other?
             * 
             * so The solution here is you put the async keyword at the method like:
             * async method1() to make the program understand this method will be invoke as asynchronization
             *   statement1
             *   a = await statement2
             *   statement3
             *   statement4 = a;
             *   
             *   this means statement1 and 3 will invoke whenever they are ready
             *   the statement2 must be completed then the statement4 will be executed later because it depends on statement2
             */
        }
    }
}
