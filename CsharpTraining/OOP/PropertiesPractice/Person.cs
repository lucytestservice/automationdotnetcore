﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.PropertiesPractice
{
    public class Person
    {
        //Use 'prop' to do some snippet

        //private DateTime _birthdate;

        //Auto-implemented property -> it will automatically create a private value for this property and then implement getter and setter for us
        public DateTime Birthdate { get; set; }


        //Put private to encapsulate the setter. Can not set it unless we use constructor
        public DateTime JoinDate { get; private set; }

        public int Age
        {
            get
            {
                var timeSpan = DateTime.Today - Birthdate;
                var years = timeSpan.Days / 365;
                return years;
            }
        }


        // classic Properties getter setter (if need some logic inside get set)
        /* public DateTime Birthdate
         {
             get { return _birthdate; }
             set { _birthdate = value; }
         }*/


        //Modern propertis getter setter style
        /*public DateTime Birthdate { get => _birthdate; set => _birthdate = value; }*/

    }
}
