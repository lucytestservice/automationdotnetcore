﻿namespace CsharpTraining.OOP.Scoupling.Composition
{
    public class Installer
    {
        private readonly LoggerClass _logger;

        public Installer(LoggerClass logger)
        {
            _logger = logger;
        }

        public void Install()
        {
            _logger.Log("Installing...");
        }
    }
}
