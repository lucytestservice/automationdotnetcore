﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsharpTraining.FileIO
{
    public class FilePractice
    {
        public static void Practice()
        {
            //File class -> static method
            //FileInfo class -> instance method

            /**
             * Create()
             * Copy() / CopyTo()
             * Delete()
             * Exist()
             * GetAttributes()
             * Move()
             * ReadAllText()
             */

            var path = @"C:\abc\ab";
            File.Copy(path, @"C:\amd\as", true); //true to override if the file exist
            File.Delete(path);

            if(File.Exists(path))
            {
                //
            }

            var content = File.ReadAllText(path);


            var file = new FileInfo(path);
            file.CopyTo(@"C:\asad\");
            file.Delete();

            //FileInfo instance just only have Exists property
            if(file.Exists)
            {

            }
        }
    }
}
