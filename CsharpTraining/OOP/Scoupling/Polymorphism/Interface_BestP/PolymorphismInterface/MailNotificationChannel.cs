﻿namespace CsharpTraining.OOP.Scoupling.Polymorphism
{
    public class MailNotificationChannel : INotificationChannel
    {
        public void Send(Message message)
        {
            System.Console.WriteLine($"Sending mail....{message}");
        }
    }
}