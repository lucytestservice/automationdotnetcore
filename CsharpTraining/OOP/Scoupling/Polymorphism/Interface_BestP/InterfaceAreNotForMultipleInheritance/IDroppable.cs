﻿namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{
    internal interface IDroppable
    {
        void Drop();
    }
}