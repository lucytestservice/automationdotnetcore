﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.ClassPractice
{
    public class CallerClass
    {
        public static void Practice()
        {
            Customer customer = new Customer();
            customer.Id = 1;
            customer.Name = "dang";

            Console.WriteLine(customer.Id);
            Console.WriteLine(customer.Name);

            Customer customer1 = new Customer(2, "phuong");
            customer1.orders.Add(new Order());
            Console.WriteLine(customer1.Id);
            Console.WriteLine(customer1.Name);
            Console.WriteLine(customer1.orders[0].Name);
        }

        public static void ObjectInitializer()
        {
            Customer customer = new Customer()
            {
                Id = 2,
                Name = "dang2",
                orders = new List<Order>()
            };
            Console.WriteLine("CALLING OBJECT INITIALIZER......");
            customer.ToString();
        }
    }
}
