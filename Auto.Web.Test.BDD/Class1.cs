﻿using System;
using Machine.Specifications;

namespace Test
{
    [Subject("TestCase1")]
    public class Class1
    {
        static string value1; 
        static string value2;

        private Establish context = () =>
        {
            value1 = "dang";
            value2 = "dang";
        };


        private Because of = () =>
            value1 = value1;


        private It should_equal = () =>
            value1.ShouldEqual(value2);
        
    }
}