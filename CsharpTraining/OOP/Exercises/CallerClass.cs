﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using CsharpTraining.OOP.Exercises.WorkFlowEngine;

namespace CsharpTraining.OOP.Exercises
{
    public class CallerClass
    {
        public static void Practice()
        {
            StopWatch.Start();
            StopWatch.Stop();
            Console.WriteLine(StopWatch.Duration);

            StopWatch.Start();
            Thread.Sleep(3000);
            StopWatch.Stop();
            Console.WriteLine(StopWatch.Duration);

            //Workflow Engine using Interface

            Console.WriteLine("Workflow Engine using Interface...........");


            WorkFlow workFlow = new WorkFlow();
            workFlow.AddActivity(new UploadVideo());
            workFlow.AddActivity(new EncodingVideo());
            workFlow.AddActivity(new SendEmailToStakeHolder());
            workFlow.AddActivity(new UpdateStatus());

            WorkflowEngine.Run(workFlow);
        }
    }
}
