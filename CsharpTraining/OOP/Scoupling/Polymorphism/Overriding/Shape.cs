﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Overriding
{

    public class Shape
    {
        public int Height { get; set; }
        public int Weight { get; set; }

        public Position position { get; set; }

        public virtual void Draw()
        {
            Console.WriteLine("default implement draw");
        }
    }
}
