﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.NullableType
{
    public class Practice
    {
        public static void Caller()
        {
            DateTime? date1 = null;
            DateTime date2 = date1 ?? DateTime.Today;

            Console.WriteLine(date2);
        }
    }
}
