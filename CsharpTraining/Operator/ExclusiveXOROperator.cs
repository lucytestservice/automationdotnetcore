﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class ExclusiveXOROperator
    {
        /**
         * The operator for an "exclusive or" (for short XOR) is: ^
            This operator returns true when one, but only one, of the supplied bools are true.
            true ^ false // Returns true
            false ^ true // Returns true
            false ^ false // Returns false
            true ^ true // Returns false
         */
    }
}
