﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.BuiltInSupport
{
    public class BuiltInMethod
    {
        public static void practice()
        {
            /**
             * Random class
             */
            var random = new Random();
            Console.WriteLine((char)random.Next(97, 122));
            Console.WriteLine((int)random.Next(97, 122));

            // Apply Random class to generate password
            var passwordLength = 10;
            var buffer = new char[passwordLength];
            for (var i = 0; i < passwordLength; i++)
            {
                buffer[i] = (char)('a' + random.Next(0, 26));
            }
            var password = new string(buffer);
            Console.WriteLine(password);
        }
    }
}
