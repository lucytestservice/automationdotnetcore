﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public class UploadVideo : IActivityController
    {
        private const string _message = "UPLOADING VIDEO TO CLOUD STORAGE....";

        public void Execute()
        {
            Console.WriteLine(_message);
        }
    }
}
