using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutoCore.UITesting
{
    public static class DriverHelper
    { 
        static WebDriverWait Wait => new WebDriverWait(DriverFactory.Instance, TimeSpan.FromSeconds(3));
        
        public static bool WaitForEnabled(this IWebDriver webdriver, IWebElement webElement)
        {
            return Wait.Until(x=> webElement.Enabled);
        }

        public static bool WaitForEnabled(this IWebDriver webdriver, IWebElement webElement, int second = 3)
        {
            return new WebDriverWait(DriverFactory.Instance, TimeSpan.FromSeconds(second))
                .Until(x=> webElement.Enabled);
        }

        public static bool WaitForDisplayed(this IWebDriver webdriver, IWebElement webElement)
        {
            return Wait.Until(x=> webElement.Displayed);
        }

        public static bool WaitForDisplayed(this IWebDriver webdriver, IWebElement webElement, int second = 3)
        {
            return new WebDriverWait(DriverFactory.Instance, TimeSpan.FromSeconds(second))
                .Until(x=> webElement.Displayed);
        }

        public static void Sleep(this IWebDriver webDriver, int second)
        {
            second *= 1000;
            Thread.Sleep(second);
        }

        // Wait for Ajax or JQuery runs completely
        public static bool WaitForReady(this IWebDriver webdriver)
        {
            return Wait.Until(driver =>
            {
                var isAjaxFinished = (bool) ((IJavaScriptExecutor) driver).ExecuteScript("return jQuery.active == 0");
                return isAjaxFinished;
            });
        }
    }
}