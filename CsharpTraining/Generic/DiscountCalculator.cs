﻿namespace CsharpTraining.Generic
{
    public class DiscountCalculator<TProduct> where TProduct : Product
    {
        public float CalculatDiscount(TProduct product)
        {
            return product.Price * 75 / 100;
        }
    }
}
