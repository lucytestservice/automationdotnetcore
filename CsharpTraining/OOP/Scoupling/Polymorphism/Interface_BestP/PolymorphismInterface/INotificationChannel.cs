﻿namespace CsharpTraining.OOP.Scoupling.Polymorphism
{
    public interface INotificationChannel
    {
        void Send(Message message);
    }
}