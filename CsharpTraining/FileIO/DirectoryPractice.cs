﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsharpTraining.FileIO
{
    public class DirectoryPractice
    {
        public static void Practice()
        {
            //Directory class -> static method
            //DirectoryInfo class -> instance method

            /**
             * CreateDirectory()
             * Delete()
             * Exist()
             * GetCurrentDirectory()
             * GetFile()
             * Move()
             * GetLogicalDrives()
             */

            //Directory.CreateDirectory(@"D:\csharp\automation2\");

            var files = Directory.GetFiles(@"D:\csharp\automation\", "*.*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                Console.WriteLine(file);
            }

            var directories = Directory.GetDirectories(@"D:\csharp", "*.*", SearchOption.AllDirectories);
            foreach ( var directory in directories)
            {
                Console.WriteLine(directory);
            }

            Console.WriteLine(Directory.Exists("..."));

            var directory2 = new DirectoryInfo(@"D:\csharp");
            Console.WriteLine(directory2.Name);
        }
    }
}
