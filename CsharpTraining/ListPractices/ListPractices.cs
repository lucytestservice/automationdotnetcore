﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.ListPractices
{
    public class ListPractices
    {
        public static void Practice()
        {
            /**
             * - Useful methods
             * Add()
             * AddRange()
             * Remove()
             * RemoveAll()
             * IndexOf()
             * Contains()
             * Clear()
             * 
             * - Property:
             * Count
             */
            var numbers = new List<int>() { 1, 2, 3 };
            numbers.Add(6);
            numbers.AddRange(new int[3] { 10, 12, 123});
            
            foreach (var val in numbers)
            {
                Console.WriteLine(val);
            }

            Console.WriteLine();

            numbers.Add(1); //add duplicated 1 value
            Console.WriteLine(numbers.IndexOf(1)); // -> 0 while there is another '1' at the end
            Console.WriteLine(numbers.LastIndexOf(1)); // -> 7 last index of '1'

            Console.WriteLine(numbers.Count);

           /* foreach(var number in numbers)
            {
                if(number == 1)
                {
                    numbers.Remove(number); //Csharp does not allow us to remove an element in foreach
                }
            }*/

        }
    }
}
