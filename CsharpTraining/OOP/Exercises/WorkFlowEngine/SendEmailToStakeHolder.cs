﻿namespace CsharpTraining.OOP.Exercises.WorkFlowEngine
{
    public class SendEmailToStakeHolder : IActivityController
    {
        private const string _message = "SENDING MESSAGE TO CUSTOMER....";

        public void Execute()
        {
            System.Console.WriteLine(_message);
        }
    }
}