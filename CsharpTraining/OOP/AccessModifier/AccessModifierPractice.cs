﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.AccessModifier
{
    public class AccessModifierPractice
    {
        /**
         * There are 5 access modifier
         * 
         * 
         * public
         * private
         * protected: derrived
         * internal: access only from the same assembly
         * protected internal: access only from derrived class only and must in the same assembly
         * 
         * 
         */

        //Private
        private DateTime _birthdate;

        public DateTime GetBirthDate()
        {
            return _birthdate;
        }

        public void SetBirthDate(DateTime value)
        {
            _birthdate = value;
        }

    }
}
