﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.PropertiesPractice
{
    public class PropertiesPractice
    {
        /**
         * Properties are fields with encapsulation getter setter
         */
        public static void Practice()
        {
            Person person = new Person();
            person.Birthdate = new DateTime(1995, 09, 09);
            Console.WriteLine(person.Age);
        }
    }
}
