﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsharpTraining.ExtensionMethods
{
    public class Caller
    {
        public static void Practice()
        {
            var quote = "Hello my name is Dang and I am a good boy";
            Console.WriteLine(quote.ShortenByNumberOfWord(5));
        }
    }

    public static class ExtensionHere
    {
        public static string ShortenByNumberOfWord(this String obj, int cutdownNum)
        {
            if (cutdownNum < 0)
                throw new ArgumentOutOfRangeException("cutdownNum must be equal or greater than zero");

            if (cutdownNum == 0)
                return "";

            var words = obj.Split(' ');

            if (words.Length <= cutdownNum)
                return obj;

            return string.Join(" ", words.Take(cutdownNum));
        }

    }
}
