﻿using System;

namespace CsharpTraining.OOP.Scoupling.Polymorphism.Interface_BestP
{
    public class TextBox : UIControl, IDraggable, IDroppable
    {
        public void Drag()
        {
            //this is implementation not inherit from Interface
            //Usually use controller as a controller
        }

        public void Drop()
        {
            //This is implementation not inherit from Interface
        }
    }
}
