﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Variable
{
    class StandardVariable
    {
        // UInteger  
        uint ui = 5U;

        // Integer
        int i = 5;

        // SByte
        sbyte sb = 127;

        // Decimal   
        decimal m = 30.5M;

        // Double
        double d = 30.5D;
        // Float
        float f = 30.5F;

        // Long
        long l = 5L;
        // ULong
        ulong ul = 5UL;

        // String literal
        string s = "hello, this is a string literal";

        // Verbatim String
        string s2 = @"The path is:
                    C:\Windows\System32";

        // Char literal. Empty char can be default(char) or new char()) is '\0' or NULL (not null keyword or null reference)
        char c = 'h';

        // Byte
        byte b = 127;


        // Short Byte
        short shortb = 127;

        // UShort Byte
        ushort us = 127;


        // Boolean
        bool result = true;
    }
}
