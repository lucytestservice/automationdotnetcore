﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Composition
{
    public class LoggerClass
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
