﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.StringPractices
{
    public class StringPractice
    {
        public static void practice()
        {
            /**
             * String class
             */

            // Static support
            string a = " ";
            Console.WriteLine(String.IsNullOrWhiteSpace(a));

            // Or
            String.IsNullOrEmpty(a);

            // Extension support
            string b = "askdhakhdakhdadkad";
            b.ToLower();
            b.ToUpper();
            b.Trim();
            b.IndexOf('a');
            b.LastIndexOf('a');
            b.Substring(4);
            b.Substring(0, 10);
            b.Replace("as","sa");
            b.Replace('a', 'b');
            b.Split(' ');

            // convert
            string c = "2";
            int.Parse(c);
            Convert.ToInt32(c);

            // ToString with pattern

            int d = 123;
            d.ToString();
            d.ToString("C"); //Format to currency $123.00
            d.ToString("C0"); //Format to currency with no decimal number

        }
    }
}
