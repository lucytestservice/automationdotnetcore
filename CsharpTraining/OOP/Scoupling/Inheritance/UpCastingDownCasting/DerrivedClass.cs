﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.OOP.Scoupling.Inheritance.UpCastingDownCasting
{
    public class DerrivedClass : BaseClass
    {
        public int FontSize { get; set; }
        public string FontStyle { get; set; }

    }
}
