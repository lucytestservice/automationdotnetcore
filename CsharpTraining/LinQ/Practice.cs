﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsharpTraining.LinQ
{
    public class Practice
    {
        public static void Caller()
        {
            var books = new BookRepository().GetBooks();

            //var cheapBooks = new List<Book>();

            //foreach (var book in books)
            //{
            //    if (book.Price < 15)
            //        cheapBooks.Add(book);
            //}

            //Using LinQ Query Operator
            var cheaperBooks = from book in books
                               where book.Price < 20
                               orderby book.Title
                               select book.Title;

            //Using LinQ Extension methods
            var cheapBooks = books
                                 .Where(book => book.Price < 20)
                                 .OrderBy(book => book.Title)
                                 .Select(book => book.Title);

            Console.WriteLine(@$"There are {cheapBooks.ToList().Count} cheap book");


            foreach (var book in cheapBooks)
            {
                Console.WriteLine($"Book title is {book}");
            }


            var specialbook = books.Single(b => b.Title.Contains("Special"));
            Console.WriteLine(specialbook.Title);

            var specialbookorDefault = books.SingleOrDefault(b => b.Title == "SpecialA");
            Console.WriteLine(specialbookorDefault == null);

            var firstBook = books.First(b => b.Title.Contains("star"));
            Console.WriteLine(firstBook.Title);

            var lastBookOrDefault = books.LastOrDefault(b => b.Title.Contains("Star"));
            Console.WriteLine(lastBookOrDefault == null);

            //Offset technique

            var skip2First = books.Skip(2).Take(3);
            foreach (var book in skip2First)
            {
                Console.WriteLine(book.Title);
            }

            Console.WriteLine(books.Count());

            Console.WriteLine(books.Max(b => b.Price));
            Console.WriteLine(books.Min(b => b.Price));
            Console.WriteLine(books.Sum(b => b.Price));

            //Average

            Console.WriteLine(books.Average(b => b.Price));
        }
    }
}
