﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DebuggerProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("To Debug the custom framework");
            var a = Assembly.LoadFrom("Auto.Web.Auto.Web.Test.BDD.dll");
            var types = a.GetTypes();
            var testClass = types.Where(t =>
                t.IsClass && t.IsPublic &&
                t.CustomAttributes.Any(att => att.AttributeType == typeof(TestClassAttribute)));
            var methods = testClass.First().GetMethods();
            methods.First().Invoke(Activator.CreateInstance(testClass.First()), null);
        }
    }
}