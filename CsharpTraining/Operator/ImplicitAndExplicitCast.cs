﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class ImplicitAndExplicitCast
    {
        public static void Practice()
        {
            var binaryImage = new BinaryImage();
            ColorImage colorImage = binaryImage; // implicit cast, note the lack of type
            bool[] pixels = (bool[])binaryImage; // explicit cast, defining the type
        }
    }

    public class BinaryImage
    {
        public bool[] _pixels;
        public static implicit operator ColorImage(BinaryImage im)
        {
            return new ColorImage(im._pixels);
        }
        public static explicit operator bool[](BinaryImage im)
        {
            return im._pixels;
        }
    }

    public class ColorImage
    {
        private bool[] _pixels;
        public ColorImage(bool[] pixels)
        {
            _pixels = pixels;
        }

        public static explicit operator BinaryImage(ColorImage ci)
        {
            return new BinaryImage()
            {
                _pixels = ci._pixels
            };
        }
    }
}
