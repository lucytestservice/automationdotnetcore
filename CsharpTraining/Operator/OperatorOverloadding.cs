﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Operator
{
    class OperatorOverloadding
    {
        /**
             * Following is the operators that could be overloadded:
             *  +, -, !, ~, ++, --	    | Những toán tử một ngôi này nhận một toán hạng và có thể được nạp chồng
             *  ----------------------------------------------------------------------------------------------
                +, -, *, /, %	        | Những toán tử nhị phân này nhận một toán hạng và có thể được nạp chồng
             *  ----------------------------------------------------------------------------------------------
                ==, !=, <, >, <=, >=    | These can not be overriden in struct unless it is overriden by Object.Equals
             */
    }
    class Box
    {
        private double chieu_dai;
        private double chieu_rong;
        private double chieu_cao;

        public Box(double a, double b, double c)
        {
            chieu_dai = a;
            chieu_rong = b;
            chieu_cao = c;
        }

        public Box()
        {

        }

        public double tinhTheTich()
        {
            return chieu_dai * chieu_rong * chieu_cao;
        }

        public void setChieuDai(double len)
        {
            chieu_dai = len;
        }

        public void setChieuRong(double bre)
        {
            chieu_rong = bre;
        }

        public void setChieuCao(double hei)
        {
            chieu_cao = hei;
        }


        /**
             * Following is the operators that could be overloadded:
             *  +, -, !, ~, ++, --	    | Những toán tử một ngôi này nhận một toán hạng và có thể được nạp chồng
             *  ----------------------------------------------------------------------------------------------
                +, -, *, /, %	        | Những toán tử nhị phân này nhận một toán hạng và có thể được nạp chồng
             *  ----------------------------------------------------------------------------------------------
                ==, !=, <, >, <=, >=    |
             */

        // nap chong toan tu + de cong hai doi tuong Box.
        // This is a user defined operator
        // Means: there is an operator + to sum 2 set of attributes from 2 Box Class. 
        public static Box operator +(Box b, Box c)
        {
            Box box = new Box();
            box.chieu_dai = b.chieu_dai + c.chieu_dai;
            box.chieu_rong = b.chieu_rong + c.chieu_rong;
            box.chieu_cao = b.chieu_cao + c.chieu_cao;
            return box;
        }

        //Serveral example
        // operator overloading == 
        public static bool operator ==(Box a,
                                       Box c)
        {
            bool result = false;
            if ((a.chieu_dai == c.chieu_dai) && (a.chieu_rong == c.chieu_rong) && (a.chieu_cao == c.chieu_cao))
            {
                result = true;
            }
            return result;
        }

        // operator overloading != 
        public static bool operator !=(Box a,
                                       Box c)
        {
            bool result = true;
            if ((a.chieu_dai == c.chieu_dai) && (a.chieu_rong == c.chieu_rong) && (a.chieu_cao == c.chieu_cao))
            {
                result = false;
            }
            return result;
        }
    }

    // Using Equatable Interfacce to handle equal object
    class Student : IEquatable<Student>
    {
        public string Name { get; set; } = ""; //getter setter with default value
        public bool Equals(Student other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(other, this)) return true;
            return string.Equals(Name, other.Name);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as Student);
        }
        public override int GetHashCode()
        {
            return Name?.GetHashCode() ?? 0;
        }
        public static bool operator ==(Student left, Student right)
        {
            return Equals(left, right);
        }
        public static bool operator !=(Student left, Student right)
        {
            return !Equals(left, right);
        }
    }
}
