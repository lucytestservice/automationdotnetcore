﻿using System;
using System.Threading;

namespace CsharpTraining.Events
{
    public class MessageService
    {
        public void OnVideoEncoded(object source, VideoEventArgs e)
        {
            Console.WriteLine("Sending message......." + e.Video.Title);
            Thread.Sleep(2000);
            Console.WriteLine("Completed sending message!");
        }
    }
}
