﻿using System;

namespace CsharpTraining.ExceptionHandling
{
    internal class Calculator
    {
        public int Divide(int numerator, int denumerator)
        {
            return numerator / denumerator;
        }
    }
}