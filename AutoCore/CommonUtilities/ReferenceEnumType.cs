﻿namespace AutoCore.CommonUtilities
{
    public class ReferenceEnumType
    {
        public string Value { get; private set; }

        protected ReferenceEnumType(string value)
        {
            Value = value;
        }
    }
}