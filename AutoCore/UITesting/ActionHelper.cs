using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace AutoCore.UITesting
{
    public class ActionHelper
    {
        private readonly Actions _actions;

        public ActionHelper(IWebDriver driver)
        {
            _actions = new Actions(driver);
        }

        public Actions UserAct => _actions;

        public void ContextClick()
        {
            _actions.ContextClick().Build().Perform();
        }

        public void ContextClick(IWebElement onElement)
        {
            _actions.ContextClick().Build().Perform();
        }

        public void PressEnter()
        {
            _actions.SendKeys(Keys.Enter).Build().Perform();
        }

        public void PressCtrlAndSendKeys(string keys)
        {
            _actions.KeyDown(Keys.Control).SendKeys(keys).KeyUp(Keys.Control).Build().Perform();
        }

        public void PressShiftAndSendKeys(string keys)
        {
            _actions.KeyDown(Keys.Shift).SendKeys(keys).KeyUp(Keys.Shift).Build().Perform();
        }

        public void PressCtrlAndClick(IWebElement webElement)
        {
            _actions.KeyDown(Keys.Control).Click(webElement).KeyUp(Keys.Control).Build().Perform();
        }

        public void PressShiftAndClick(IWebElement webElement)
        {
            _actions.KeyDown(Keys.Shift).Click(webElement).KeyUp(Keys.Shift).Build().Perform();
        }

        public void DragAndDrop(IWebElement fromElement, IWebElement toElement)
        {
            
        }

        public void DragAndDropBy(IWebElement fromElement, int xOffset, int yOffset) {}

        public void MoveToElement(IWebElement toElement) {}
        public void MoveByOffset(IWebElement fromElement, int xOffset, int yOffset) {}
        public void Release() {}
        public void ReleaseToElement() {}

        public void SlideTo(IWebElement webElement, int targetValue, int maxValue, int minValue)
        {
            var pixelX = webElement.Size.Width;

            _actions.MoveToElement(
                    webElement,
                    pixelX * (targetValue - minValue) / (maxValue - minValue),
                    0)
                .Click().Build().Perform();
        }
    }
}