﻿using System.Collections.Generic;
using System.Text;

namespace CsharpTraining.Events
{
    public class Practice
    {
        public static void Caller()
        {
            var video = new Video() { Title = "video1" };
            var videoEncoder = new VideoEncoder(); //This is the publisher
            var mailService = new MailService(); //This is the subscriber
            var messageService = new MessageService();

            //mailservice subscribe to VideoEncoded Event
            // Or you can understand as is the videoEncoder shoot an event to notify the subscriber
            videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded += messageService.OnVideoEncoded;

            videoEncoder.Encode(video);
        }
    }
}
